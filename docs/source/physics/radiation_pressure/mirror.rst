.. include:: /defs.hrst
.. _radiation_pressure_mirror:

Radiation Pressure at a Mirror
******************************

When light is incident on a mirror's surface, it generates a force pushing on the
mirror. In the steady state, we may assume that any constant light field is accounted
for by control systems, such that the only effect will be due to the beats between
different frequency light fields e.g. a carrier and its signal sidebands.

The couplings between optical fields and surface motions are given below.

Surface motion to optical field coupling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Longitudinal Motion
```````````````````

In the case of longitudinal motion, the generated signal sidebands :math:`a_{s,jnm}^\pm`
of a carrier field :math:`a_{c,j}` are simply :cite:`phd.Brown`

.. math::
    a_{s,jnm}^\pm = i r k Z_s^\pm\sum_{n',m'}a_{c,jn'm'}K_{nmn'm'}^0

where :math:`Z_s^+ \equiv Z_s` and :math:`Z_s^- \equiv Z_s^*` are the surface motion
along the beam axis, and :math:`K_{nmn'm'}^0` is the HOM coupling due to a static mirror
distortion. Note that this is for the side of the mirror defined as "positive" i.e. the
port1 (`p1`) side. For the opposite side (`p2`), the longitudinal motion is negative, so
an extra 180° phase is added.

In |Finesse|, we must also propagate the carrier field to and the generated field away
from the mirror's surface by the mirror's static tuning :math:`\phi`. The overall
couplings for a carrier field :math:`a_{c,j}` with frequency offset :math:`f_{c,j}` and
signal field :math:`a_{s,j}` with frequency offset :math:`f_{s,j}` are then

.. math::
    a_{s,jnm}^\pm = e^{i\phi(1 + \frac{f_{s,j}}{f_0} + \frac{f_{c,j}}{f_0})}
        i r k Z_s^\pm\sum_{n',m'}a_{c,jn'm'}K_{nmn'm'}^0

for the positive side and

.. math::
    a_{s,jnm}^\pm = -e^{-i\phi(1 + \frac{f_{s,j}}{f_0} + \frac{f_{c,j}}{f_0})}
        i r k Z_s^\pm\sum_{n',m'}a_{c,jn'm'}K_{nmn'm'}^0

for the negative, where :math:`f_0` is the simulation's default frequency.


Optical field to surface motion coupling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Longitudinal Motion
```````````````````

As laid out in :cite:`phd.Brown`, the radiation pressure due to a pair of signal
sidebands beating against a carrier field is given by

.. math::
    F_\mathrm{rp}(\Omega) = \frac{2}{c}(-P_{1i}(\Omega) - P_{1o}(\Omega) +
    P_{2i}(\Omega) + P_{2o}(\Omega)),

where

.. math::
    P(\Omega) = \sum_j\sum_{n,m}(a_{s,jnm}^+ a_{c,jnm}^* + a_{s,jnm}^{-*} a_{c,jnm})

is the oscillating power at the signal frequency :math:`\Omega`. This is then muliplied
by the mechanical susceptibility :math:`H(\Omega)`, along with any other external
longitudinal forces on the mirror, to give the total mirror motion

.. math::
    Z_s(\Omega) = H(\Omega) \left[
        \sum_n^{N_F}F_{\mathrm{rp},n}(\Omega) + F_\mathrm{ext}(\Omega)
        \right].
