.. include:: /defs.hrst
.. _tracing_manual:

Tracing the beam
****************

In general the Gaussian beam parameter of a :math:`\M{00}` mode is changed at every
optical surface (see :ref:`transforming_beam_param`).  In other words, for each location
inside the interferometer where field amplitudes are to be computed a certain beam
parameter has to be set for the simulation.

A possible method to find reasonable beam parameters for every location in the
interferometer (every node in |Finesse|) is to first set only some specific beam
parameters and then derive the remaining beam parameters from these initial ones:
usually it is sensible to assume that the beam at the input can be properly described by
the (hopefully known) beam parameter of the laser's output mode. In addition, in most
cavities the light fields can be described safely by using cavity eigenmodes.

Algorithm details
~~~~~~~~~~~~~~~~~

.. todo::

    Rewrite this section now that beam tracing is done very differently to when this
    section was originally written. This should contain details, with examples, of how
    the binary tree structure and related algorithms work.
